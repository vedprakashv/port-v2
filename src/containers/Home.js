import React from 'react'

import Banner from '../components/Banner'
import Nav from '../components/Nav'
import Services from '../components/Services';
import About from '../components/About'
import Expert from '../components/Expert'
import ContactMe from '../components/ContactMe'
import Footer from '../components/Footer'


const Home = () => {
    return (

        <>
            <Banner />
            <Nav />
            <Services />
            <About />
            <Expert />
            <ContactMe />
            <Footer />
        </>
    );
}

export default Home
