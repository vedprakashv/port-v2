import React from 'react'
import AbtImg from '../data/about2.png'

const About = () => {
    return (
        <>
            <div className="about" id="about">
                <div className="head_box text-center">
                    <h2>About Me</h2>
                    {/* <p>Lorem Ipsum</p> */}
                    <p className="underline"></p>
                </div>

                <div className="about_content">
                    <div className="col-md-5 about__content_left">
                        <img src={AbtImg} className="img-fluid rounded" />
                    </div>

                    <div className=" col-md-5 about_content_right ">
                        <div className="">
                            <h3>Hi There</h3>
                            <p>
                                Loremlsdkfjsdlkjfsdlkfjsdlkfjsdl
                                kfjsdlkfjsldnkscvnsknvs
                            </p>
                            <p>fsjdfsdlfjsdlfjds
                            skjfsldjflkj</p>
                            <p>sdfjsdfjlsdfsldjfds</p>
                        </div>

                        <div>
                            <table className="table text-light">
                                <th>
                                    <tr>
                                        <td className="text-danger ">
                                            Name: <tr className="h6 text-light">Ved Prakash Vishwakarma</tr>
                                        </td>

                                        <td className="text-danger ">
                                            E-mail: <tr className="h6 text-light">vedvprakash25@gmail.com</tr>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td className="text-danger ">
                                            Phone: <tr className="h6 text-light">+91 7987745506</tr>
                                        </td>

                                        <td className="text-danger ">
                                            LinkedIn: <tr className="h6 text-light">
                                                <a href="https://www.linkedin.com/in/
                                            vedprakash-vishwakarma-a6a825140" className="text-decoration-none text-light">Click here</a></tr>
                                        </td>
                                    </tr>
                                </th>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}

export default About
