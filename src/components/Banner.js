import React from 'react'
import { FaFacebook, FaTwitter, FaLinkedin } from 'react-icons/fa';
import { GoMarkGithub } from 'react-icons/go';
import Resume from '../data/resume.pdf'

const Banner = () => {
    function resumeTab(props) {
        window.open(Resume);
    }
    return (

        <header className="header" >
            <div className="container" >
                <div className="row" >

                    <div className="header__content " >
                        <div className="header__section ">
                            <div >
                                <ul className="header__ul p-0 col-md-6 " >
                                    <li > <a href="https://www.facebook.com/ved.p.vishwa" class="shadow"> < FaFacebook /> </a></li>
                                    <li > <a href="https://twitter.com/home?lang=en" class="shadow">< FaTwitter /></a> </li>
                                    <li > <a href="https://www.linkedin.com/in/vedprakash-vishwakarma-a6a825140/" class="shadow">< FaLinkedin /></a> </li>
                                    <li > <a href="https://github.com/ved1998karma" class="shadow"> < GoMarkGithub /></a> </li>
                                </ul>
                            </div>
                            <div className="banner_title">
                                <h1 className=" " > I am Ved Prakash Vishwakarma </h1>
                                <p className="h5 text-muted"> i, m Ved, web developer </p>
                            </div>

                            <div>
                                <button className="btn hire-btn btn-outline-danger">
                                    <a href="#" className="text-decoration-none text-light" onClick={resumeTab} >Resume</a>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </header>
    )
}

export default Banner