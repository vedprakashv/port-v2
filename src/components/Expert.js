import React from 'react'

const Expert = () => {
    return (
        <>
            <div id="expert" className="expert_box d-flex justify-content-center text-light">
                
                <div className="col-md-4 expert_content">
                    <p className="text-danger h5"> I, M EXPERT ON</p>
                    <h1>Lets work together</h1>
                    <button className="hire-btn btn btn-outline-danger btn-lg"><a href="/contactme" className="text-decoration-none text-light">Hire me now</a></button>
                </div>



                <div className="expert_bar col-md-5 ">
                    <ul className="list-unstyled">
                        <li>HTML and CSS
                            <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{ width: '90%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>JavaScript(ES6+)
                            <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{ width: '65%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>React (Currently Learning)
                            <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{ width: '60%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>Node.js (Currently Learning)
                            <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{ width: '40%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>Bootstrap
                            <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{ width: '75%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>

                    </ul>
                </div>

            </div>

        </>
    )
}

export default Expert
