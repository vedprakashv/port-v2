import React from 'react'
import { FaEnvelope, FaLinkedin, FaLinkedinIn, FaPhoneAlt } from 'react-icons/fa'

const ContactMe = () => {
    return (
        <>
            <div id="contactme" className="contact pt-5 text-light">

                <div className="text-center ">
                    <p className="h6 text-danger">HAVE ANY QUERY</p>
                    <h2>Contact Me</h2>
                    <div className="underline">

                    </div>
                </div>

                <div className="d-flex col-md-9 justify-content-center m-auto">
                    <div className="contact_cards col-md-3 ">
                        <FaEnvelope className="text-danger h3" />
                        <div className=" px-3 ">
                            <p className="h5">Email Me</p>
                            <p className="">vedvprakash25</p>

                        </div>
                    </div>
                    <div className="contact_cards col-md-3 ">
                        <FaPhoneAlt className="text-danger h3" />
                        <div className=" px-3 " >
                            <p className="h5"> Call Me</p>
                            <p className="">+91 7987745506</p>

                        </div>
                    </div>
                    <div className="contact_cards col-md-3 ">
                        <FaLinkedinIn className="text-danger h3" />
                        <div className=" px-3 ">
                            <p className="h5"> Join Me</p>
                            <p className=""><a href="https://www.linkedin.com/in/vedprakash-vishwakarma-a6a825140/" className="text-decoration-none text-secondary">linkedin</a></p>

                        </div>
                    </div>
                </div>
                <div className="contact-form text-light col-md-10 offset-1">
                    <form method="POST" className="p-4 pb-5">
                        <div className="d-flex justify-content-around p-4">
                            <div className="form-group col-md-3 col-sm-3">
                                <input type="text" className="form-control" placeholder="Your Name" name="name" aria-label="Username" aria-describedby="basic-addon1" />
                            </div>
                            <div className="form-group col-md-3 col-sm-3">
                                <input type="email" className="form-control" placeholder="Your Email" name="email" aria-label="Username" aria-describedby="basic-addon1" />
                            </div>
                            <div className="form-group col-md-3 col-sm-3">
                                <input type="text" className="form-control" placeholder="Your Subject " name="phone" aria-label="Username" aria-describedby="basic-addon1" />
                            </div>

                        </div>

                        <div className="p-4 pt-5">
                            <textarea className="form-control p-3 col-md-6 col-sm-6 " placeholder="Message" name="message" aria-label="With textarea"></textarea>
                        </div>

                        <div className="text-center">
                            <button className="hire-btn btn btn-outline-danger btn-lg">
                                <a href="/contactme" className="text-decoration-none text-light"> Send Message</a>
                            </button>
                        </div>
                    </form>
                </div>
            </div>


        </>
    )
}

export default ContactMe
