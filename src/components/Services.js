import React from 'react'
import { GoMarkGithub } from 'react-icons/go'
import { FaCodepen } from 'react-icons/fa';
// import { FiFigma } from 'react-icons/fi'

function Services() {
    return (
        <>
            <div id="services" className="service pt-5 text-center text-light">
                <p className="h5 text-danger">SERVICES</p>
                <h1 className="h3">My Services</h1>
                <div className="underline">

                </div>
            </div>

            <div className="cards p-5 d-flex flex-wrap ">
                <div className="card col-md-5 text-light">
                    <GoMarkGithub className="service-icons" />
                    <h4>Web Development</h4>
                    <p>Lorem ipsumsjhfjhsdfjsdfklj
                    ashaskjdhasdh
                    shjashdjaksdh
                    ahdasdhaskjh
                    </p>
                </div>
                <div className="card col-md-5 text-light">
                    < FaCodepen className="service-icons mb-3" />
                    <h4>Web Design</h4>
                    <p>Lorem ipsumsjhfjhsdfjsdfklj
                    ashaskjdhasdh
                    shjashdjaksdh
                    ahdasdhaskjh
                    </p>
                </div>
                {/* <div className="card col-md-5 text-light">
                    < FaFileVideo className="service-icons" />
                    <h4>Video Editing</h4>
                    <p>Lorem ipsumsjhfjhsdfjsdfklj
                    ashaskjdhasdh
                    shjashdjaksdh
                    ahdasdhaskjh
                    </p>
                </div> */}
            </div>
        </>
    )
}

export default Services
