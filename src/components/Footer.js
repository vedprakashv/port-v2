import React from 'react'
import { FaFacebook, FaTwitter,  FaLinkedin ,FaEnvelope} from 'react-icons/fa';
import { GoMarkGithub } from 'react-icons/go';


const Footer = () => {
    return (
        <>
            <footer className="footer_section text-light text-center pb-5 head_box ">
                
                    <div className="ml-2">
                        <h4 className=""><FaEnvelope size={35}/> Email me</h4>
                        <h4>vedvprakash25@gmail.com</h4>
                    </div>

                    <div className=" p-2 my-3">
                        <ul className="footer__ul col-md-4 pt-5 m-auto " >
                            <li > <a href="https://www.facebook.com/ved.p.vishwa" className="shadow"> < FaFacebook /> </a></li>
                            <li > <a href="https://twitter.com/home?lang=en" className="shadow">< FaTwitter /></a> </li>
                            <li > <a href="https://www.linkedin.com/in/vedprakash-vishwakarma-a6a825140/" className="shadow">< FaLinkedin /></a> </li>
                            <li > <a href="https://github.com/ved1998karma" className="shadow"> < GoMarkGithub /></a> </li>
                        </ul>
                        
                    </div>
                    <h6 className="head_box">Copyright &copy; All Rights Reserved</h6>
                
            </footer>
        </>
    )
}

export default Footer
