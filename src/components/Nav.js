import React from 'react';
import Logo from '../data/logo.jpg';

const Nav = () => {
    return (
        <nav className="navbar">
            <div className="container">
                <div className="navbar__container">
                    <ul className="navbar__left">
                        <div className="navbar__left-logo">
                            <img src={Logo} alt="logo" />
                        </div>
                    </ul>
                    <ul className="navbar__right">
                        <li><a href="/">Home</a></li>
                        <li><a href="/#services">Services</a></li>
                        <li><a href="/#about">About</a></li>
                        <li><a href="/#expert">Skills</a></li>
                        <li><a href="/#contactme">Contact</a></li>
                    </ul>
                </div>


            </div>
        </nav>

    )
}

export default Nav
